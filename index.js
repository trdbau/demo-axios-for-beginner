var country = {};
var selectHtmlCode = document.getElementById("select");

axios
	.get("https://restcountries.eu/rest/v2/all")
	.then(response => {
		country = response.data;
	})
	.then(() => {
		renderData();
	});

function renderData() {
	for (const key in country) {
		if (country.hasOwnProperty(key)) {
			selectHtmlCode.innerHTML =
				selectHtmlCode.innerHTML + `<option>${country[key].name}</option>`;
		}
	}
}
